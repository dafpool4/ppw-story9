from django.shortcuts import render
from django.http import JsonResponse
import urllib.request
import json
# Create your views here.

url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
def data(request):
    
    req = urllib.request.Request(url)

    read = urllib.request.urlopen(req).read()
    my_dict = json.loads(read.decode('utf-8'))
    return JsonResponse(my_dict)

def search(request):
	querry = request.POST.get('usr_querry', "usr_querry")
	url = 'https://www.googleapis.com/books/v1/volumes?q='+querry
	req = urllib.request.Request(url)
	
	read = urllib.request.urlopen(req).read()
	my_dict = json.loads(read.decode('utf-8'))
	return JsonResponse(my_dict)
	
def index(request):
    return render(request, "landing.html")
    